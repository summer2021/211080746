from mars.dataframe.reduction import DataFrameSum, DataFrameProd, DataFrameMin, \
    DataFrameMax, DataFrameCount, DataFrameMean, DataFrameVar, DataFrameAll, \
    DataFrameAny, DataFrameSkew, DataFrameKurtosis, DataFrameSem, \
    DataFrameAggregate, DataFrameCummin, DataFrameCummax, DataFrameCumprod, \
    DataFrameCumsum, DataFrameNunique, CustomReduction
from mars.tests import setup

import numpy as np
import pandas as pd
import pytest
from mars.dataframe import DataFrame, Series
import mars.tensor as mt


def testMax(setup):
    a = DataFrame({"a":[1,1,1,1],"b":[1,1,1,1],"c":[1,1,1,1],"d":[1,1,1,1]},chunk_size=2)
    b = a.sum()
    print(b.execute())