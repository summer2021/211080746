# Copyright 1999-2021 Alibaba Group Holding Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from typing import Union

import numpy as np
import pandas as pd
import mars.dataframe as md

from ...core import OutputType, recursive_tile
from ..operands import DataFrameOperand, DataFrameOperandMixin
from ...serialization.serializables import BoolField, AnyField
from .core import DataFrameReductionOperand, DataFrameReductionMixin
from ..utils import build_empty_series


def _drop_str(x):
    if isinstance(x, (int, float)):
        return x


class DataFrameMedian(DataFrameReductionOperand, DataFrameReductionMixin):
    _func_name = 'median'

    axis = AnyField('axis')
    skipna = BoolField('skipna')
    level = AnyField('level')
    numeric_only = BoolField('numeric_only')

    def __init__(self, axis=None, skipna=None, level=None, numeric_only=None, **kws):
        super().__init__(axis=axis, skipna=skipna, level=level, numeric_only=numeric_only, **kws)
        self.output_types = [OutputType.scalar]

    @property
    def is_atomic(self):
        return True

    @classmethod
    def tile(cls, op):
        inp = op.inputs[0]
        out = op.outputs[0]
        if len(inp.chunks) == 1:
            output_type = OutputType.scalar
            dtypes, index = out.dtype, None
            out_df = yield from recursive_tile(inp.agg(
                cls.get_reduction_callable(op), axis=op.axis or 0, _numeric_only=op.numeric_only,
                _output_type=output_type, _dtypes=dtypes, _index=index
            ))
            return [out_df]
        else:
            nan_num = yield from recursive_tile(inp.isna().sum())
            if op.numeric_only:
                inp = inp.apply(_drop_str)
            else:
                inp = md.to_numeric(inp)

            inp = inp.sort_values(ignore_index=True)
            if op.skipna or op.skipna is None:
                inp = inp.dropna()
            real_size = yield from recursive_tile(inp.count())
            inp = yield from recursive_tile(inp)
            chunks = []
            for c in inp.chunks:
                chunk_op = op.copy().reset_key()
                chunk_param = c.params
                chunk_param['shape'] = ()
                c = [c]
                c.extend(real_size.chunks)
                chunk = chunk_op.new_chunk(c, kws=[chunk_param])
                chunks.append(chunk)
            chunks.append(nan_num.chunks[0])
            combine_op = DataFrameCombineMedian(op.skipna)
            combine_param = chunks[0].params
            combine_param['shape'] = ()
            combine_chunks = combine_op.new_chunk(chunks, [combine_param])

            new_op = op.copy()
            kw = out.params.copy()
            kw['chunks'] = [combine_chunks]
            kw['nsplits'] = ()
            a = new_op.new_tileables(op.inputs, kws=[kw])
            return a

    @classmethod
    def execute(cls, ctx, op):
        inp = ctx[op.inputs[0].key]
        real_size = ctx[op.inputs[1].key]
        indexs = []
        if real_size % 2 == 0:
            indexs.append(real_size / 2)
            indexs.append(real_size / 2 - 1)
        else:
            indexs.append(real_size // 2)
        result = []
        for idx in indexs:
            if idx in inp.index:
                result.append(inp.loc[idx])
        result = np.array(result)
        ctx[op.outputs[0].key] = result

    def __call__(self, data):
        empty_series = build_empty_series(data)
        d = np.array(empty_series).dtype
        return self.new_scalar([data], dtype=d)


def median_series(data, axis=None, skipna=None, level=None, numeric_only=None):
    if axis is not None and axis != 0:
        raise ValueError(f'ValueError: No axis named {axis} for object type Series')
    if level != 0:
        pass

    op = DataFrameMedian(axis, skipna, level, numeric_only)
    return op(data)


class DataFrameCombineMedian(DataFrameOperand, DataFrameOperandMixin):
    skipna = BoolField('skipna')

    def __init__(self, skipna=None, **kws):
        super().__init__(skipna=skipna, **kws)
        self.output_types = [OutputType.scalar]

    @classmethod
    def execute(cls, ctx, op):
        nan_num = ctx[op.inputs[-1].key]
        if nan_num != 0 and op.skipna is not None and not op.skipna:
            ctx[op.outputs[0].key] = np.nan
        else:
            total_inp = []
            for inp in op.inputs[:-1]:
                total_inp.extend(ctx[inp.key])
            ctx[op.outputs[0].key] = sum(total_inp) / len(total_inp)
