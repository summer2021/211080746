# Copyright 1999-2021 Alibaba Group Holding Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from typing import Union

import numpy as np
import pandas as pd

from ...core import OutputType, recursive_tile
from ...core.operand import OperandStage
from ..core import SERIES_TYPE
from ..datasource.dataframe import from_pandas as from_pandas_df
from ..datasource.series import from_pandas as from_pandas_series
from ..initializer import Series as asseries
from ..operands import DataFrameOperand, DataFrameOperandMixin
from ...core.context import Context
from ...serialization.serializables import StringField, AnyField, Int64Field
from ...typing import OperandType
from ...config import options
from ...utils import ceildiv
from ..utils import indexing_index_value, is_cudf
from ..merge import DataFrameConcat
from ..indexing.iloc import DataFrameIlocGetItem




class DataFrameNlargest(DataFrameOperand, DataFrameOperandMixin):
    n = Int64Field('n')
    columns = AnyField('columns')
    keep = StringField('keep')

    def __init__(self, n=None, columns=None, keep='first', **kws):
        super().__init__(n=n, columns=columns, keep=keep, **kws)
        self.output_types = [OutputType.dataframe]

    @classmethod
    def tile(cls, op):
        inp = op.inputs[0]
        out = op.outputs[0]

        if len(inp.chunks) == 1:
            chunk_op = op.copy().reset_key()
            chunk_param = out.params
            chunk_param['index'] = (0, 0)
            chunk = chunk_op.new_chunk(inp.chunks, kws=[chunk_param])

            new_op = op.copy()
            param = out.params
            param['chunks'] = [chunk]
            param['nsplits'] = ((np.nan,), (np.nan,))
            return new_op.new_dataframe(op.inputs, kws=[param])
        else:
            inp = inp.sort_values(by=op.columns, ascending=False)
            # head_op = DataFrameHeadWithKeep(n=op.n, keep=op.keep)
            # inp = head_op(inp)
            # op = DataFrameIlocGetItem()

            ret = yield from recursive_tile(inp)
            chunks = []
            for c in ret.chunks:
                chunk_op = DataFrameNlargest(
                    n=op.n,
                    columns=op.columns,
                    keep=op.keep,
                    stage=OperandStage.map)
                chunk_params = c.params

                chunk_params['shape'] = (np.nan, np.nan)
                chunks.append(chunk_op.new_chunk([c], kws=[chunk_params]))
            new_op = op.copy()
            params = out.params
            params['chunks'] = chunks
            params['nsplits'] = ((np.nan,), (np.nan,))
            return new_op.new_dataframe(out.inputs, kws=[params])


    @classmethod
    def execute(cls, ctx, op):
        if op.stage != OperandStage.map:
            in_data = ctx[op.inputs[0].key]
            result = in_data.nlargest(op.n, op.columns, op.keep)
        else:
            result = ctx[op.inputs[0].key]
        ctx[op.outputs[0].key] = result

    def __call__(self, a):
        if a.ndim == 2:
            return self.new_dataframe([a], shape=(np.nan, np.nan), dtypes=a.dtypes,
                                      index_value=None,
                                      columns_value=a.columns_value)
        else:
            return self.new_series([a], shape=(np.nan, np.nan), dtype=a.dtype,
                                   index_value=None, name=a.name)


class DataFrameHeadWithKeep(DataFrameOperand, DataFrameOperandMixin):
    n = Int64Field('n')
    keep = StringField('keep')

    def __init__(self, n=None, keep='first', **kws):
        super().__init__(n=n, keep=keep, **kws)
        if not self.output_types:
            self.output_types = [OutputType.dataframe]

    def __call__(self, a):
        if a.ndim == 2:
            return self.new_dataframe([a], shape=(np.nan, np.nan), dtypes=a.dtypes,
                                      index_value=None,
                                      columns_value=a.columns_value)
        else:
            return self.new_series([a], shape=(np.nan, np.nan), dtype=a.dtype,
                                   index_value=None, name=a.name)

    @classmethod
    def tile(cls, op):
        inp = op.inputs[0]
        out = op.outputs[0]
        combine_size = options.combine_size

        chunks = inp.chunks

        new_chunks = []
        for c in chunks:
            chunk_op = op.copy().reset_key()
            params = out.params
            params['index'] = c.index
            params['shape'] = c.shape if np.isnan(c.shape[0]) else out.shape
            new_chunks.append(chunk_op.new_chunk([c], kws=[params]))
        chunks = new_chunks

        while len(chunks) > 1:
            new_size = ceildiv(len(chunks), combine_size)
            new_chunks = []
            for i in range(new_size):
                in_chunks = chunks[combine_size * i: combine_size * (i + 1)]
                chunk_index = (i, 0) if in_chunks[0].ndim == 2 else (i,)
                if len(inp.shape) == 1:
                    shape = (sum(c.shape[0] for c in in_chunks),)
                else:
                    shape = (sum(c.shape[0] for c in in_chunks), in_chunks[0].shape[1])
                concat_chunk = DataFrameConcat(
                    axis=0, output_types=in_chunks[0].op.output_types).new_chunk(
                    in_chunks, index=chunk_index, shape=shape)
                chunk_op = op.copy().reset_key()
                params = out.params
                params['index'] = chunk_index
                params['shape'] = in_chunks[0].shape if np.isnan(in_chunks[0].shape[0]) else out.shape
                new_chunks.append(chunk_op.new_chunk([concat_chunk], kws=[params]))
            chunks = new_chunks

        new_op = op.copy()
        params = out.params
        params['nsplits'] = tuple((s,) for s in out.shape)
        params['chunks'] = chunks
        return new_op.new_tileables(op.inputs, kws=[params])

    @classmethod
    def execute(cls, ctx, op):
        chunk = op.outputs[0]
        df = ctx[op.inputs[0].key]
        if len(op.inputs) > 1:
            indexes = tuple(ctx[index.key] if hasattr(index, 'key') else index
                            for index in op.indexes)
        else:
            indexes = tuple(op.n)
        r = df.iloc[indexes]
        if isinstance(r, pd.Series) and r.dtype != chunk.dtype:
            r = r.astype(chunk.dtype)
        if is_cudf(r):  # pragma: no cover
            r = r.copy()
        ctx[chunk.key] = r


def df_nlargest(data, n, columns, keep="first"):
    """
    Return the first `n` rows ordered by `columns` in descending order.

    Return the first `n` rows with the largest values in `columns`, in
    descending order. The columns that are not specified are returned as
    well, but not used for ordering.

    This method is equivalent to
    ``df.sort_values(columns, ascending=False).head(n)``, but more
    performant.

    Parameters
    ----------
    n : int
        Number of rows to return.
    columns : label or list of labels
        Column label(s) to order by.
    keep : {'first', 'last', 'all'}, default 'first'
        Where there are duplicate values:

        - `first` : prioritize the first occurrence(s)
        - `last` : prioritize the last occurrence(s)
        - ``all`` : do not drop any duplicates, even it means
                    selecting more than `n` items.

    Returns
    -------
    DataFrame
        The first `n` rows ordered by the given columns in descending
        order.

    See Also
    --------
    DataFrame.nsmallest : Return the first `n` rows ordered by `columns` in
        ascending order.
    DataFrame.sort_values : Sort DataFrame by the values.
    DataFrame.head : Return the first `n` rows without re-ordering.

    Notes
    -----
    This function cannot be used with all column types. For example, when
    specifying columns with `object` or `category` dtypes, ``TypeError`` is
    raised.

    Examples
    --------
    >>> df = pd.DataFrame({'population': [59000000, 65000000, 434000,
    ...                                   434000, 434000, 337000, 11300,
    ...                                   11300, 11300],
    ...                    'GDP': [1937894, 2583560 , 12011, 4520, 12128,
    ...                            17036, 182, 38, 311],
    ...                    'alpha-2': ["IT", "FR", "MT", "MV", "BN",
    ...                                "IS", "NR", "TV", "AI"]},
    ...                   index=["Italy", "France", "Malta",
    ...                          "Maldives", "Brunei", "Iceland",
    ...                          "Nauru", "Tuvalu", "Anguilla"])
    >>> df
              population      GDP alpha-2
    Italy       59000000  1937894      IT
    France      65000000  2583560      FR
    Malta         434000    12011      MT
    Maldives      434000     4520      MV
    Brunei        434000    12128      BN
    Iceland       337000    17036      IS
    Nauru          11300      182      NR
    Tuvalu         11300       38      TV
    Anguilla       11300      311      AI

    In the following example, we will use ``nlargest`` to select the three
    rows having the largest values in column "population".

    >>> df.nlargest(3, 'population')
            population      GDP alpha-2
    France    65000000  2583560      FR
    Italy     59000000  1937894      IT
    Malta       434000    12011      MT

    When using ``keep='last'``, ties are resolved in reverse order:

    >>> df.nlargest(3, 'population', keep='last')
            population      GDP alpha-2
    France    65000000  2583560      FR
    Italy     59000000  1937894      IT
    Brunei      434000    12128      BN

    When using ``keep='all'``, all duplicate items are maintained:

    >>> df.nlargest(3, 'population', keep='all')
              population      GDP alpha-2
    France      65000000  2583560      FR
    Italy       59000000  1937894      IT
    Malta         434000    12011      MT
    Maldives      434000     4520      MV
    Brunei        434000    12128      BN

    To order by the largest values in column "population" and then "GDP",
    we can specify multiple columns like in the next example.

    >>> df.nlargest(3, ['population', 'GDP'])
            population      GDP alpha-2
    France    65000000  2583560      FR
    Italy     59000000  1937894      IT
    Brunei      434000    12128      BN
    """

    op = DataFrameNlargest(n, columns, keep)
    return op(data)

def series_nlargest(data, n=5, keep="first"):
    """
            Return the largest `n` elements.

            Parameters
            ----------
            n : int, default 5
                Return this many descending sorted values.
            keep : {'first', 'last', 'all'}, default 'first'
                When there are duplicate values that cannot all fit in a
                Series of `n` elements:

                - ``first`` : return the first `n` occurrences in order
                    of appearance.
                - ``last`` : return the last `n` occurrences in reverse
                    order of appearance.
                - ``all`` : keep all occurrences. This can result in a Series of
                    size larger than `n`.

            Returns
            -------
            Series
                The `n` largest values in the Series, sorted in decreasing order.

            See Also
            --------
            Series.nsmallest: Get the `n` smallest elements.
            Series.sort_values: Sort Series by values.
            Series.head: Return the first `n` rows.

            Notes
            -----
            Faster than ``.sort_values(ascending=False).head(n)`` for small `n`
            relative to the size of the ``Series`` object.

            Examples
            --------
            >>> countries_population = {"Italy": 59000000, "France": 65000000,
            ...                         "Malta": 434000, "Maldives": 434000,
            ...                         "Brunei": 434000, "Iceland": 337000,
            ...                         "Nauru": 11300, "Tuvalu": 11300,
            ...                         "Anguilla": 11300, "Montserrat": 5200}
            >>> s = pd.Series(countries_population)
            >>> s
            Italy       59000000
            France      65000000
            Malta         434000
            Maldives      434000
            Brunei        434000
            Iceland       337000
            Nauru          11300
            Tuvalu         11300
            Anguilla       11300
            Montserrat      5200
            dtype: int64

            The `n` largest elements where ``n=5`` by default.

            >>> s.nlargest()
            France      65000000
            Italy       59000000
            Malta         434000
            Maldives      434000
            Brunei        434000
            dtype: int64

            The `n` largest elements where ``n=3``. Default `keep` value is 'first'
            so Malta will be kept.

            >>> s.nlargest(3)
            France    65000000
            Italy     59000000
            Malta       434000
            dtype: int64

            The `n` largest elements where ``n=3`` and keeping the last duplicates.
            Brunei will be kept since it is the last with value 434000 based on
            the index order.

            >>> s.nlargest(3, keep='last')
            France      65000000
            Italy       59000000
            Brunei        434000
            dtype: int64

            The `n` largest elements where ``n=3`` with all duplicates kept. Note
            that the returned Series has five elements due to the three duplicates.

            >>> s.nlargest(3, keep='all')
            France      65000000
            Italy       59000000
            Malta         434000
            Maldives      434000
            Brunei        434000
            dtype: int64
            """

    op = DataFrameNlargest(n, keep=keep)
    return op(data)
