from ..add1 import *
from ..sum import *
import pytest
import mars.tensor as mt
from mars.tests import setup


def testAddOne(setup):
    t = mt.ones((4, 4), chunk_size=2)
    added = add_one(t)
    sumed = sum(added)
    print(sumed.execute())