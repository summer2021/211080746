from mars.tensor.operands import TensorOperand, TensorOperandMixin

class TensorSum(TensorOperand, TensorOperandMixin):
    @classmethod
    def tile(cls, op):
        inp = op.inputs[0]
        out_chunks = []
        # for c in inp.chunks:
        #     chunk_op = op.copy().reset_key()
        #     new_chunk = chunk_op.new_chunk([c], **c.params.copy())
        #     out_chunks.append(new_chunk)
        new_op = op.copy()
        chunk_op = op.copy().reset_key()
        out_chunks.append(chunk_op.new_chunk(inp.chunks,**inp.chunks[0].params.copy()))
        kw = op.outputs[0].params
        nsplits = ((kw["shape"][0],),(kw["shape"][1],))

        a = new_op.new_tensor(op.inputs, chunks=out_chunks,
                                 nsplits=nsplits,
                                 **kw)
        return a

    @classmethod
    def execute(cls, ctx, op):
        chunk_data = ctx[op.inputs[0].key]
        res = chunk_data
        ctx[op.outputs[0].key] = res


def sum(t):
    op = TensorSum()
    added = op.new_tensor([t], **t.params.copy())
    return added



