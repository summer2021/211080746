from mars.tensor.operands import TensorOperand, TensorOperandMixin

class TensorAddOne(TensorOperand, TensorOperandMixin):
    @classmethod
    def tile(cls, op):
        inp = op.inputs[0]
        out_chunks = []
        for c in inp.chunks:
            chunk_op = op.copy().reset_key()
            new_chunk = chunk_op.new_chunk([c], **c.params.copy())
            out_chunks.append(new_chunk)
        new_op = op.copy()
        a = new_op.new_tensor(op.inputs, chunks=out_chunks,
                                 nsplits=op.inputs[0].nsplits,
                                 **op.outputs[0].params)
        return a

    @classmethod
    def execute(cls, ctx, op):
        chunk_data = ctx[op.inputs[0].key]
        res = chunk_data + 1
        ctx[op.outputs[0].key] = res


def add_one(t):
    op = TensorAddOne()
    sumed = op.new_tensor([t], **t.params.copy())
    return sumed



